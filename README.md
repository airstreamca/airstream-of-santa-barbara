Airstream of Santa Barbara is an Airstream-only dealership located in Buellton, CA, proudly serving the needs of the Central Coast and Southern California. We offer a diverse selection of the latest models, floor plans and trim levels of Airstream Trailers and Touring Coaches.

Address: 404 E Hwy 246, Buellton, CA 93427, USA

Phone: 805-697-7989

Website: https://www.airstreamofsantabarbara.com

